<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?php
	include("title.html");
	require("secondTitle.html");

	echo("Ciao, i'm the first echo!");
	echo '<br><strong>Ciao</strong>, sono il secondo <em>echo!</em>';

	$name = "Paolo";
	$surname = "Calvaresi";
	$age = 34;

	echo '</br>'.$name.' '.$surname.' '.$age;

	include("type.html");
	include("form.html");
	include("conditional.html");
	include("functions.php");
	include("iteractions.php");
	include("example.php");
?>
	
</body>
</html>